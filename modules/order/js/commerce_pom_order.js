(function ($) {
  Drupal.behaviors.commercePomOrder = {
    attach: function (context, settings) {
      setupProductAutocomplete(context, settings);

      // Prevent the form from getting submitted if
      // the search box is in focus
      $('.commerce-pom-product-search').keydown(function (event) {
        if (event.keyCode == 13 && $('.commerce-pom-product-search').is(':focus')) {
          event.preventDefault();
        }
      });

      // Show ajax throbber while we're searching.
      $('.commerce-pom-product-search').keyup(function() {
        if ($(this).val() != '') {
          $(this).addClass('show-ajax-throbber');
        }
        else {
          $(this).removeClass('show-ajax-throbber');
        }
      });

      // Key Navigator Stuff
      $('commerce-pom-product-autocomplete .commerce-pom-order-product-display', context).keynavigator({
        activateOn: 'click',
        parentFocusOn: 'mouseover'
      });

      /**
       * Attaches some custom autocomplete functionality to the product search field.
       */
      function setupProductAutocomplete(context, settings) {
        $('.commerce-pom-product-autocomplete', context).each(function () {
          var element = $(this);

          element.once('commerce-pom-autocomplete', function () {
            element.autocomplete({
              source: settings.commercePomOrder.productAutoCompleteUrl,
              focus: function (event, ui) {
                event.preventDefault(); // without this: keyboard movements reset the input to ''

                // clear all other rows as selected
                $('.commerce-pom-product-search-item').removeClass('selected');
                ui.item.element.addClass('selected');
              },
              select: function (event, ui) {
                var sku = ui.item.element.find('.btn-add').attr('data-product-sku');

                if (Drupal.settings.commercePomOrder.autoCompleteCallback) {
                  Drupal[Drupal.settings.commercePomOrder.autoCompleteNamespace][Drupal.settings.commercePomOrder.autoCompleteCallback](sku);
                }
                else {
                  addProductSku(sku);
                }
              },
              context: this
            });

            // Override the default UI autocomplete render function.
            element.data('ui-autocomplete')._renderItemData = function (ul, item) {
              var product = $("<li></li>");

              item.element = product;

              product.addClass('commerce-pom-product-search-item')
                .data("ui-autocomplete-item", item)
                .append(item.markup)
                .appendTo(ul)
                .find('.btn-add').click(function (e) {
                e.preventDefault();
                e.stopPropagation(); // prevent product from being added via bubbling

                var sku = $(this).attr('data-product-sku');

                if (Drupal.settings.commercePomOrder.autoCompleteCallback) {
                  Drupal[Drupal.settings.commercePomOrder.autoCompleteNamespace][Drupal.settings.commercePomOrder.autoCompleteCallback](sku);
                }
                else {
                  addProductSku(sku);
                }

                element.data('ui-autocomplete').close();

              });

              // Remove the ajax throbber now that we've got all products.
              $('.commerce-pom-product-search').removeClass('show-ajax-throbber');

              return product;
            };
            // Overide renderMenu to sort the list of autocomplete results
            element.data('ui-autocomplete')._renderMenu = function (ul, items) {
              // Sort the items by title in an inline comparison function
              items.sort(function (a, b) {
                return a.title.localeCompare(b.title)
              });
              $.each(items, function (index, item) {
                element.data('ui-autocomplete')._renderItemData(ul, item);
              });
            }
          });
        });
      }

      /**
       * Populates the product SKU field on the form and triggers its AJAX event.
       */
      function addProductSku(sku) {
        $('.commerce-pom-product-sku-input')
          .val(sku)
          .trigger('blur');
      }

      /**
       * Scroll the page to the products table when the 'Add to P.O.' button is clicked.
       */
      $('input#edit-bulk-add-products-products-add-to-po').click(function () {
        $('html, body').animate({
          scrollTop: $('#pom-products-table').offset().top
        }, 2000);
      });

      /** Display the send vendor email checkbox if the vendor email textfield is
       *  filled. #states won't work because vendor email is being filled via ajax.
       *  TODO: Ugliness galore, we need to find a better way to do this!
       */
      if (!$('input[name=vendor_email]').val()) {
        $('.form-item-send-email-to-vendor').hide();
      }
      else {
        $('.form-item-send-email-to-vendor').show();
      }
      $('input[name=vendor_email]').on('input', function() {
        if (!$('input[name=vendor_email]').val()) {
          $('.form-item-send-email-to-vendor').hide();
        }
        else {
          $('.form-item-send-email-to-vendor').show();
        }
      });

      /**
       * Display the activity log when the activity log link is clicked.
       */
      $('#activity-log-button').click(function(event) {
        event.preventDefault();
        if (!$('#pom-activity-log-view').is(':animated')) {
          $('#pom-activity-log-view').slideToggle('slow');
        }
      });

      /**
       * Display the new activity log fields when the add not link is clicked.
       */
      $('#add-a-note-link').click(function(event) {
        event.preventDefault();
        if (!$('#pom-new-activity-log').is(':animated')) {
          $('#pom-new-activity-log').slideToggle('slow');
        }
      });
      // Fix for when we have validation errors for this field.
      if ($('#pom-new-activity-log .form-textarea').hasClass('error')) {
        $('#pom-new-activity-log').show();
      }

      /**
       * Confirm that the user wants to really send the Purchase Order
       * to the vendor.
       */
      $('.pom-submit-purchase-order').click(function(e, showDialog = true) {
        if (showDialog && Drupal.settings.commerce_pom_order.message.length > 0) {
          e.preventDefault();
          $("#pom-dialog-message").html($(Drupal.settings.commerce_pom_order.message));
          $("#pom-dialog-message").dialog({
            modal: true,
            resizable: false,
            width: 400,
            dialogClass: 'pom-order-dialog',
            buttons: {
              "Yes, send P.O.": function() {
                $(this).dialog("close");
                $('.pom-submit-purchase-order').trigger('click', [false]);
              },
              "No, don't send": function() {
                $(this).dialog("close");
                return false;
              }
            }
          });
        }
      });

      /**
       * Trigger the remove all products button click when the 'Remove All'
       * button is clicked.
       */
      $('#pom-remove-all-products', context).click(function(event){
        event.preventDefault();

        $('.commerce-pom-remove-all-products').trigger('remove_all_products');
      });
    }
  }
}(jQuery));
