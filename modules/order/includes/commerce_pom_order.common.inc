<?php

/**
 * @file
 * Common callbacks and functionality for commerce_pom.
 */

/**
 * Callback for the product autocomplete.
 */
function commerce_pom_order_product_autocomplete() {
  $params = drupal_get_query_parameters();
  $products = [];

  if (!empty($params['term'])) {
    $results = commerce_pom_order_product_search($params['term']);

    // Let's return an empty results message back when we get no products.
    if (empty($results)) {
      $products[-1] = _commerce_pom_order_product_get_empty_results_message();
    }

    foreach ($results as $product_id) {
      if ($data = _commerce_pom_order_product_autocomplete_build($product_id)) {
        $products[$product_id] = $data;
      }
    }
  }

  drupal_json_output($products);
}

/**
 * Searches for products via a keyword search.
 */
function commerce_pom_order_product_search($keywords) {
  // First try and perform a search through the Search API.
  if (!($results = commerce_pom_order_product_search_api_search($keywords))) {
    $results = commerce_pom_order_product_search_basic($keywords);
  }

  return $results;
}

/**
 * Uses the Search API to perform a product keyword search.
 */
function commerce_pom_order_product_search_api_search($keywords) {
  $results = [];

  // Check if an index has been selected.
  if ($index_id = variable_get('commerce_pom_search_api_index')) {
    if ($index_id != 'default') {
      $index = search_api_index_load($index_id);
      $query = new SearchApiQuery($index);
      $query->condition("status", 1);
      $query->keys($keywords);
      $query->range(0, variable_get('commerce_pom_search_results_count', 5));
      $query_results = $query->execute();

      if (!empty($query_results['results'])) {
        $results = array_keys($query_results['results']);
      }
    }
  }

  return $results;
}

/**
 * Perform a very basic product search via the database.
 */
function commerce_pom_order_product_search_basic($keywords) {
  $results = [];

  $query = db_select('commerce_product', 'cp');
  $query->fields('cp', ['product_id', 'title', 'type']);
  $query->condition("status", 1);
  $or = db_or();
  $or->condition('title', '%' . db_like($keywords) . '%', 'LIKE')
    ->condition('sku', $keywords, '=');
  $query->condition($or);
  $query->range(0, variable_get('commerce_pom_search_results_count', 5))
    ->orderBy('title', 'ASC');
  $result = $query->execute();

  foreach ($result as $row) {
    $results[] = $row->product_id;
  }

  return $results;
}

/**
 * Get all nodes that reference a particular brand and category taxonomy term.
 *
 * @param array $bundles
 *   The bundle types to search for.
 * @param int $brand_tid
 *   The brand tid value the node references.
 * @param int $category_tid
 *   The category tid value the node references.
 *
 * @return array
 *   An array of loaded nodes.
 */
function commerce_pom_order_get_referenced_nodes(array $bundles, $brand_tid, $category_tid) {
  $nodes = [];

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', $bundles, 'IN');
  if (!empty($brand_tid)) {
    $query->fieldCondition(variable_get('commerce_pom_brand_field', ''), 'tid', $brand_tid, 'IN');
  }
  if (!empty($category_tid)) {
    $query->fieldCondition(variable_get('commerce_pom_category_field', ''), 'tid', $category_tid, 'IN');
  }

  $result = $query->execute();
  $nids = array_keys($result['node']);
  $nodes = entity_load('node', $nids);

  return $nodes;
}

/**
 * Get the name of all commerce product reference fields a node type references.
 *
 * @param array $bundle_types
 *   The node bundle types to search for.
 *
 * @return array
 *   An array of commerce product reference fields.
 */
function commerce_pom_order_get_product_reference_fields(array $bundle_types) {
  // First, get the product reference field for our selected product display
  // bundle types.
  $fields = [];
  foreach ($bundle_types as $type) {
    $instances = field_info_instances('node', $type);
    foreach ($instances as $instance) {
      $field = field_info_field($instance['field_name']);
      if ($field['type'] == 'commerce_product_reference') {
        $fields[$instance['field_name']] = $field;
      }
    }
  }

  return $fields;
}

/**
 * Get the commerce products that a node references.
 *
 * @param array $nodes
 *   An array of node objects.
 * @param array $fields
 *   The commerce product reference fields to search for.
 *
 * @return array
 *   An array of products keyed on the product id.
 */
function commerce_pom_order_get_node_products(array $nodes, array $fields) {
  $products = [];

  foreach ($nodes as $node) {
    foreach ($fields as $field_name => $field) {
      if (isset($node->$field_name) && !empty($node->$field_name)) {
        $node_wrapper = entity_metadata_wrapper('node', $node);

        foreach ($node_wrapper->$field_name as $delta => $product_wrapper) {
          $products[$product_wrapper->product_id->value()] = $product_wrapper->value();
        }
      }
    }
  }

  return $products;
}

/**
 * Returns the vocabulary name that a field in a node type references.
 *
 * @param array $bundles
 *   The node types.
 * @param string $field_name
 *   The field name we're looking for in the node.
 *
 * @return string
 *   The name of the vocabulary.
 */
function commerce_pom_order_fetch_vocabulary_name(array $bundles, $field_name) {
  $category_vocabulary_name = '';

  foreach ($bundles as $bundle_type) {
    $instances = field_info_instances('node', $bundle_type);
    foreach ($instances as $instance) {
      if ($instance['field_name'] == $field_name) {
        $field = field_info_field($instance['field_name']);
        $category_vocabulary_name = $field['settings']['allowed_values'][0]['vocabulary'];
      }
    }
  }

  return $category_vocabulary_name;
}

/**
 * Fetches and generates the output of the PO that's sent to a vendor.
 *
 * @param object $purchase_order
 *   The purchase order entity.
 * @param string $view_type
 *   If we're displaying the info in the PO entity form or for pdf, email, etc.
 * @param array $line_item_data
 *   An array with the details of the line items in the PO.
 *
 * @return string
 *   Returns the html of the PO that's sent to the vendor.
 */
function commerce_pom_order_generate_vendor_po_display($purchase_order, $view_type = 'pdf', array $line_item_data = NULL) {
  $output = '';

  // Output the summary info for this PO.
  $output .= commerce_pom_order_get_po_summary_text($purchase_order, $view_type);

  // Load the line item info for this PO if it's not available.
  if (!$line_item_data) {
    $line_item_data = commerce_pom_order_fetch_po_line_items($purchase_order);
  }

  // Generate table for line items.
  $output .= commerce_pom_order_theme_line_items_table($purchase_order, $view_type, $line_item_data);

  // Add contact message at the end.
  $output .= '<div id="pom-po-contact-message">' . variable_get('commerce_pom_po_contact_text', '') . '</div>';

  return $output;
}

/**
 * Generates a PDF browser version of the PO that's sent to the vendor.
 *
 * @param object $purchase_order
 *   The purchase order entity.
 */
function commerce_pom_order_generate_po_pdf($purchase_order) {
  $output = commerce_pom_order_generate_vendor_po_display($purchase_order);
  $time = REQUEST_TIME;
  $filename = 'PO-' . $purchase_order->purchase_order_id . '_' . date('m-d-y', $time);
  drupal_add_css(drupal_get_path('module', 'commerce_pom') . '/css/commerce_pom_style.css');
  pdf_using_mpdf_api($output, $filename, 0);
}
