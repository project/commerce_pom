<?php

/**
 * @file
 * The controller for the purchase order entity containing the CRUD operations.
 */

/**
 * CommercePurchaseOrderControllerInterface definition.
 *
 * We create an interface here because anyone could come along and
 * use hook_entity_info_alter() to change our controller class.
 * We want to let them know what methods our class needs in order
 * to function with the rest of the module, so here's a handy list.
 *
 * @see hook_entity_info_alter()
 */
interface POMOrderControllerInterface extends DrupalEntityControllerInterface {

  /**
   * Create an entity.
   */
  public function create();

  /**
   * Save an entity.
   *
   * @param object $purchase_order
   *   The entity to save.
   */
  public function save($purchase_order);

  /**
   * Delete entities by purchase order ID.
   *
   * @param array $purchase_order_ids
   *   The entity to delete.
   */
  public function delete(array $purchase_order_ids);

}

/**
 * The controller class for commerce purchase orders.
 *
 * Contains methods for the commerce purchase order CRUD operations. Mainly
 * relies on the EntityAPIController class provided by the Entity module, just
 * overrides specific features.
 */
class POMOrderController extends DrupalDefaultEntityController implements POMOrderControllerInterface {

  /**
   * Create and return a new pom_order entity.
   */
  public function create() {
    $purchase_order = new stdClass();
    $purchase_order->type = 'pom_order';
    $purchase_order->bundle_type = 'pom_order';
    $purchase_order->pom_order_id = NULL;
    $purchase_order->purchase_order_id = '';
    $purchase_order->uid = 0;
    $purchase_order->vendor_email = '';
    $purchase_order->status = COMMERCE_POM_ORDER_STATUS_DEFAULT;
    $purchase_order->created = 0;
    $purchase_order->ordered = 0;
    return $purchase_order;
  }

  /**
   * Saves the custom fields using drupal_write_record().
   *
   * @param object $purchase_order
   *   The commerce purchase order entity.
   *
   * @return object
   *   The saved commerce purchase order entity.
   */
  public function save($purchase_order) {
    // Add the purchase order created date if it's a new po.
    if (empty($purchase_order->pom_order_id)) {
      $purchase_order->created = time();
    }

    // Invoke hook_entity_presave().
    module_invoke_all('entity_presave', $purchase_order, 'pom_order');
    $primary_keys = $purchase_order->pom_order_id ? 'pom_order_id' : [];
    drupal_write_record('pom_order', $purchase_order, $primary_keys);
    $invocation = 'entity_insert';

    // Determine if we're inserting or updating and call the appropriate
    // handlers.
    if (empty($primary_keys)) {
      field_attach_insert('pom_order', $purchase_order);
    }
    else {
      field_attach_update('pom_order', $purchase_order);
      $invocation = 'entity_update';
    }
    // Invoke either hook_entity_update() or hook_entity_insert().
    module_invoke_all($invocation, $purchase_order, 'pom_order');
    return $purchase_order;
  }

  /**
   * Deletes multiple commerce purchase order entities by ID.
   *
   * @param array $purchase_order_ids
   *   An array of commerce purchase order IDs to delete.
   *
   * @return bool
   *   TRUE on success, FALSE otherwise.
   */
  public function delete(array $purchase_order_ids) {
    if (!empty($purchase_order_ids)) {
      $purchase_orders = $this->load($purchase_order_ids, []);

      $this->deleteMultiple($purchase_orders);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Delete one or more commerce purchase order entities.
   *
   * Deletion is unfortunately not supported in the base
   * DrupalDefaultEntityController class.
   *
   * @param array $purchase_orders
   *   An array of entity IDs or a single numeric ID.
   *
   * @throws Exception
   */
  public function deleteMultiple(array $purchase_orders) {
    $pom_order_ids = [];

    if (!empty($purchase_orders)) {
      foreach ($purchase_orders as $purchase_order) {
        try {
          // Invoke hook_entity_delete().
          module_invoke_all('entity_delete', $purchase_order, 'pom_order');
          field_attach_delete('pom_order', $purchase_order);
          $pom_order_ids[] = $purchase_order->pom_order_id;

          db_delete('pom_order')
            ->condition('pom_order_id', $pom_order_ids, 'IN')
            ->execute();
        }
        catch (Exception $e) {
          watchdog_exception('commerce_pom_order', $e);
          throw $e;
        }
      }
    }
  }

}
