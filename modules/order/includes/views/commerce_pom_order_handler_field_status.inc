<?php

/**
 * Class commerce_pom_order_handler_field_status.
 */
class CommercePomOrderHandlerFieldStatus extends views_handler_field_numeric {

  /**
   * Return the human name of the status.
   */
  public function render($values) {
    $statuses = commerce_pom_order_fetch_order_statuses();

    return $statuses[$values->pom_order_status]['status_name'];
  }

}
