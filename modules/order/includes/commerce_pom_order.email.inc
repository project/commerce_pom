<?php

/**
 * @file
 * Emails for commerce_pom.
 */

/**
 * Sends an email to the vendor with the purchase order details.
 *
 * @param object $purchase_order
 *   The purchase order entity.
 * @param array $line_item_data
 *   An array with the details of the line items in the PO.
 */
function commerce_pom_order_send_vendor_email($purchase_order, array $line_item_data) {
  module_load_include('inc', 'commerce_pom_order', 'includes/commerce_pom_order.common');

  // Generate the table for line items for the email.
  $email_table_output = commerce_pom_order_theme_line_items_table($purchase_order, 'email', $line_item_data);
  // Generate the output for the PDF.
  $pdf_output = commerce_pom_order_generate_vendor_po_display($purchase_order);
  // Email summary text.
  $email_summary_text = commerce_pom_order_get_po_summary_text($purchase_order, 'email');

  // Load the vendor info.
  $term = taxonomy_term_load($purchase_order->pom_vendor[LANGUAGE_NONE][0]['tid']);

  // Create our PDF.
  $time = REQUEST_TIME;
  $filename = 'PO-' . $purchase_order->purchase_order_id . '_' . date('m-d-y', $time);
  pdf_using_mpdf_api($pdf_output, $filename, 2, FALSE);

  // Create an attachment of the PDF for the email.
  $attachment = array(
    'filecontent' => file_get_contents('public://pdf_using_mpdf/' . $filename . '.pdf'),
    'filename' => $filename . '.pdf',
    'filemime' => 'application/pdf',
  );

  // Compose the email essentials.
  $key = 'commerce_pom_order';
  $to = $purchase_order->vendor_email;
  $from = variable_get('site_mail', '');

  // Now compose the body.
  $message = $email_summary_text;
  $message .= $email_table_output;
  $message .= variable_get('commerce_pom_po_contact_text', '');

  // Add the email parameters.
  $params = array(
    'subject' => t('Purchase Order @purchase_order_id from @site_name', array(
      '@purchase_order_id' => $purchase_order->purchase_order_id,
      '@site_name' => variable_get('site_name', ''),
    )),
    'body' => $message,
    'headers' => array(
      'Bcc' => variable_get('commerce_pom_email_bcc_address', variable_get('site_mail', '')),
    ),
    'email_summary_text' => $email_summary_text,
    'email_table_output' => $email_table_output,
    'purchase_order' => $purchase_order,
    'line_item_data' => $line_item_data,
    'attachment' => $attachment,
    'vendor' => $term->name,
  );

  // Now, send the mail.
  $result = drupal_mail('commerce_pom_order', $key, $to, language_default(), $params, $from);

  if ($result['result'] == TRUE) {
    drupal_set_message(t('Email to vendor for Purchase Order @purchase_order_id has been successfully sent.', array(
      '@purchase_order_id' => $purchase_order->purchase_order_id,
    )));
  }
  else {
    watchdog('commerce_pom_order', 'Email not sent for Purchase Order @purchase_order_id.', ['@purchase_order_id' => $purchase_order->purchase_order_id], NULL, WATCHDOG_NOTICE);
    drupal_set_message(t('There was an error. The email was not sent for Purchase Order @purchase_order_id', ['@purchase_order_id' => $purchase_order->purchase_order_id]));
  }
}
