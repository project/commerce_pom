<?php

/**
 * @file
 * Contains code for creating message logs for commerce_pom_order.module.
 */

/**
 * Log a message activity whenever a purchase order is created or updated.
 *
 * @param object $purchase_order
 *   The purchase order entity.
 * @param array $data
 *   An array of info essential to the PO. ie. line item data, product data...
 * @param bool $new
 *   If the purchase order is new.
 */
function _commerce_pom_order_create_po_activity_log($purchase_order, array $data, $new = FALSE) {
  $message = message_create('commerce_pom_purchase_order_activity', array('uid' => $purchase_order->uid));

  $wrapper = entity_metadata_wrapper('message', $message);
  $wrapper->pom_purchase_order->set($purchase_order);

  $activity_log_message = _commerce_pom_order_compose_po_log_message($purchase_order, $data, $new);

  $wrapper->pom_activity_summary->set(array('value' => $activity_log_message, 'format' => 'full_html'));
  $wrapper->save();
}

/**
 * Compose the activity summary message for this purchase order save.
 *
 * @param object $purchase_order
 *   The purchase order entity.
 * @param array $data
 *   An array of info essential to the PO. ie. line item data, product data...
 * @param bool $new
 *   If the purchase order is new.
 *
 * @return string
 *   The entire message compilation of the activity summary.
 */
function _commerce_pom_order_compose_po_log_message($purchase_order, array $data, $new = FALSE) {
  $line_item_data = $data['line_item_data'];
  $previous_po = $data['previous_po'];
  $invoice_number_added = $data['invoice_number_added'];
  $products = $data['products'];

  $activity_log_message = '';

  // If we've got a new PO.
  if ($new) {
    $activity_log = t('Purchase Order @purchase_order_id created.', array(
      '@purchase_order_id' => $purchase_order->pom_order_id,
    ));

    $activity_log_message .= _commerce_pom_order_return_status_changed_message($purchase_order->status);
  }
  // Else if a PO was updated.
  else {
    // If status changed.
    if ($previous_po->status != $purchase_order->status) {
      $activity_log_message .= _commerce_pom_order_return_status_changed_message($purchase_order->status, TRUE);
    }

    // If invoice number was added.
    if ($invoice_number_added) {
      $activity_log_message .= t('<div class="pom-log-entry pom-log-invoice-added">Invoice Number Added: @invoice_number.</div>', array(
        '@invoice_number' => $invoice_number_added,
      ));
    }

    // If quantities have changed.
    $activity_log_message .= _commerce_pom_order_return_quantities_changed_message($line_item_data, $products);
  }

  return $activity_log_message;
}

/**
 * Returns a summary message about the current status.
 *
 * @param string $status
 *   The current status of the purchase order.
 * @param bool $changed
 *   If the status changed during this save.
 *
 * @return string
 *   The activity summary about the current status.
 */
function _commerce_pom_order_return_status_changed_message($status, $changed = FALSE) {
  // Get all statuses.
  $statuses = commerce_pom_order_fetch_order_statuses();

  // If the status was changed, return a different message.
  if ($changed) {
    return t('<div class="pom-log-entry pom-log-status">Status changed: @status.</div>', array(
      '@status' => $statuses[$status]['status_name'],
    ));
  }

  return t('<div class="pom-log-entry pom-log-status">Status: @status.</div>', array(
    '@status' => $statuses[$status]['status_name'],
  ));
}

/**
 * Returns a summary message about the items quantities being received.
 *
 * @param array $line_item_data
 *   The entire purchase order line item data.
 * @param array $products
 *   The current form state values of the line item products.
 *
 * @return string
 *   The activity summary about the item quantities..
 */
function _commerce_pom_order_return_quantities_changed_message(array $line_item_data, array $products) {
  $activity_log_message = '';

  // Add an entry if quantity was received during this save.
  foreach ($products as $sku => $product) {
    if (!empty($product['quantity_received'])) {
      // Get the sum of quantity received for this product.
      $previous_quantity_received = 0;
      if (!empty($line_item_data['line_item_info'][$sku]['quantity_received_items'])) {
        foreach ($line_item_data['line_item_info'][$sku]['quantity_received_items'] as $quantity_received_item) {
          $previous_quantity_received += $quantity_received_item->quantity;
        }
      }

      // Now add the log for this product.
      $activity_log_message .= t('<div class="pom-log-entry pom-log-quantity-changed">Item @product_title quantity changed from @previous_quantity to @new_quantity.</div>', array(
        '@product_title' => $line_item_data['line_item_info'][$sku]['product']->title,
        '@previous_quantity' => (int) $previous_quantity_received,
        '@new_quantity' => (int) ($previous_quantity_received + $product['quantity_received']),
      ));
    }
  }

  return $activity_log_message;
}
