<?php

/**
 * @file
 * Hooks related to commerce_pom_order module.
 */

/**
 * Allows users to change the status of a PO before it is saved to the db.
 *
 * @param object $purchase_order
 *   The purchase order entity.
 * @param array $form_state
 *   The form state array with the user values from the purchase order form.
 * @param int $status
 *   The status of the purchase order before it is saved to the database.
 */
function hook_commerce_pom_order_status_alter($purchase_order, array $form_state, &$status) {
  if ($purchase_order->status == COMMERCE_POM_ORDER_STATUS_DEFAULT) {
    $status = COMMERCE_POM_ORDER_STATUS_ORDERED;
  }
}

/**
 * Allows users to change the summary info that this displayed for each PO.
 *
 * @param object $purchase_order
 *   The purchase order entity.
 * @param string $view_type
 *   If we're displaying the table in the PO entity form or for pdf, email, etc.
 * @param string $summary_info_text
 *   The summary info to be displayed summarizing the PO and it's main details.
 */
function hook_commerce_pom_order_summary_info_alter($purchase_order, $view_type = 'form', &$summary_info_text) {
  if ($view_type == 'pdf') {
    $statuses = commerce_pom_order_fetch_order_statuses();
    $summary_info_text .= t('<h4>Purchase Order Status: @status</h4>', [
      '@status' => $statuses[$purchase_order->status]['status_name'],
    ]);
  }
}

/**
 * Allows altering the header and rows before displaying the line items of a PO.
 *
 * @param object $purchase_order
 *   The purchase order entity.
 * @param array $data
 *   The line item and product details of the PO.
 * @param string $view_type
 *   If we're displaying the table in the PO entity form or for pdf, email, etc.
 * @param array $header
 *   The headers for the line item theme table.
 * @param array $rows
 *   The rows for the line item theme table.
 */
function hook_commerce_pom_order_theme_line_item_table_alter($purchase_order, array $data, $view_type = 'form', array &$header, array &$rows) {
  if ($view_type == 'pdf') {
    $header['new_col'] = t('New Column');
    // Then, modify the rows...
  }
}
