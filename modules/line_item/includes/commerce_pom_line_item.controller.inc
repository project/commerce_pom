<?php

/**
 * @file
 * The controller for the pom line item entity.
 */

/**
 * POMLineItemControllerInterface definition.
 *
 * We create an interface here because anyone could come along and
 * use hook_entity_info_alter() to change our controller class.
 * We want to let them know what methods our class needs in order
 * to function with the rest of the module, so here's a handy list.
 *
 * @see hook_entity_info_alter()
 */
interface POMLineItemControllerInterface extends DrupalEntityControllerInterface {

  /**
   * Create an entity.
   */
  public function create();

  /**
   * Save an entity.
   *
   * @param object $line_item
   *   The entity to save.
   */
  public function save($line_item);

  /**
   * Delete entities by line item ID.
   *
   * @param array $line_item_ids
   *   The entity to delete.
   */
  public function delete(array $line_item_ids);

}

/**
 * The controller class for pom line items.
 *
 * Contains methods for the pom line items CRUD operations. Mainly relies on the
 * EntityAPIController class provided by the Entity module, just overrides
 * specific features.
 */
class POMLineItemController extends DrupalDefaultEntityController implements POMLineItemControllerInterface {

  /**
   * Create and return a new pom_line_item entity.
   */
  public function create() {
    $line_item = new stdClass();
    $line_item->type = 'pom_line_item';
    $line_item->bundle_type = 'pom_line_item';
    $line_item->pom_line_item_id = NULL;
    $line_item->pom_order_id = '';
    $line_item->quantity_ordered = 0;
    return $line_item;
  }

  /**
   * Saves the custom fields using drupal_write_record().
   *
   * @param object $line_item
   *   The pom line item entity.
   *
   * @return object
   *   The saved pom line item entity.
   */
  public function save($line_item) {
    // Invoke hook_entity_presave().
    module_invoke_all('entity_presave', $line_item, 'pom_line_item');
    $primary_keys = $line_item->pom_line_item_id ? 'pom_line_item_id' : [];
    drupal_write_record('pom_line_item', $line_item, $primary_keys);
    $invocation = 'entity_insert';

    // Determine if we're inserting or updating and call the appropriate
    // handlers.
    if (empty($primary_keys)) {
      field_attach_insert('pom_line_item', $line_item);
    }
    else {
      field_attach_update('pom_line_item', $line_item);
      $invocation = 'entity_update';
    }
    // Invoke either hook_entity_update() or hook_entity_insert().
    module_invoke_all($invocation, $line_item, 'pom_line_item');
    return $line_item;
  }

  /**
   * Deletes multiple pom line item entities by ID.
   *
   * @param array $line_item_ids
   *   An array of pom line item IDs to delete.
   *
   * @return bool
   *   TRUE on success, FALSE otherwise.
   */
  public function delete(array $line_item_ids) {
    if (!empty($line_item_ids)) {
      $line_items = $this->load($line_item_ids, []);

      $this->deleteMultiple($line_items);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Delete one or more pom line item entities.
   *
   * Deletion is unfortunately not supported in the base
   * DrupalDefaultEntityController class.
   *
   * @param array $line_items
   *   An array of entity IDs or a single numeric ID.
   *
   * @throws Exception
   */
  public function deleteMultiple(array $line_items) {
    $pom_line_item_ids = [];

    if (!empty($line_items)) {
      foreach ($line_items as $line_item) {
        try {
          // Invoke hook_entity_delete().
          module_invoke_all('entity_delete', $line_item, 'pom_line_item');
          field_attach_delete('pom_line_item', $line_item);
          $pom_line_item_ids[] = $line_item->pom_line_item_id;

          db_delete('pom_line_item')
            ->condition('pom_line_item_id', $pom_line_item_ids, 'IN')
            ->execute();
        }
        catch (Exception $e) {
          watchdog_exception('commerce_pom_line_item', $e);
          throw $e;
        }
      }
    }
  }

}
