<?php

/**
 * @file
 * The controller for the pom quantity received entity.
 */

/**
 * POMQuantityReceivedControllerInterface definition.
 *
 * We create an interface here because anyone could come along and
 * use hook_entity_info_alter() to change our controller class.
 * We want to let them know what methods our class needs in order
 * to function with the rest of the module, so here's a handy list.
 *
 * @see hook_entity_info_alter()
 */
interface POMQuantityReceivedControllerInterface extends DrupalEntityControllerInterface {

  /**
   * Create an entity.
   */
  public function create();

  /**
   * Save an entity.
   *
   * @param object $quantity_received
   *   The entity to save.
   */
  public function save($quantity_received);

  /**
   * Delete entities by quantity received ID.
   *
   * @param array $quantity_received_ids
   *   The entity to delete.
   */
  public function delete(array $quantity_received_ids);

}

/**
 * The controller class for pom quantity received items.
 *
 * Contains methods for the pom quantity received items CRUD operations. Mainly
 * relies on the EntityAPIController class provided by the Entity module, just
 * overrides specific features.
 */
class POMQuantityReceivedController extends DrupalDefaultEntityController implements POMQuantityReceivedControllerInterface {

  /**
   * Create and return a new pom_quantity_received entity.
   */
  public function create() {
    $quantity_received = new stdClass();
    $quantity_received->type = 'pom_quantity_received';
    $quantity_received->bundle_type = 'pom_quantity_received';
    $quantity_received->uid = 0;
    $quantity_received->pom_qty_received_id = NULL;
    $quantity_received->pom_line_item_id = '';
    $quantity_received->invoice_number = '';
    $quantity_received->quantity = 0;
    $quantity_received->received = 0;
    return $quantity_received;
  }

  /**
   * Saves the custom fields using drupal_write_record().
   *
   * @param object $quantity_received
   *   The pom quantity received entity.
   *
   * @return object
   *   The saved pom quantity received entity.
   */
  public function save($quantity_received) {
    // Invoke hook_entity_presave().
    module_invoke_all('entity_presave', $quantity_received, 'pom_quantity_received');
    $primary_keys = $quantity_received->pom_qty_received_id ? 'pom_qty_received_id' : [];
    drupal_write_record('pom_quantity_received', $quantity_received, $primary_keys);
    $invocation = 'entity_insert';

    // Determine if we're inserting or updating and call the appropriate
    // handlers.
    if (empty($primary_keys)) {
      field_attach_insert('pom_quantity_received', $quantity_received);
    }
    else {
      field_attach_update('pom_quantity_received', $quantity_received);
      $invocation = 'entity_update';
    }
    // Invoke either hook_entity_update() or hook_entity_insert().
    module_invoke_all($invocation, $quantity_received, 'pom_quantity_received');
    return $quantity_received;
  }

  /**
   * Deletes multiple pom quantity received entities by ID.
   *
   * @param array $quantity_received_ids
   *   An array of pom quantity received IDs to delete.
   *
   * @return bool
   *   TRUE on success, FALSE otherwise.
   */
  public function delete(array $quantity_received_ids) {
    if (!empty($quantity_received_ids)) {
      $quantity_received_items = $this->load($quantity_received_ids, []);

      $this->deleteMultiple($quantity_received_items);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Delete one or more pom quantity received entities.
   *
   * Deletion is unfortunately not supported in the base
   * DrupalDefaultEntityController class.
   *
   * @param array $quantity_received_items
   *   An array of entity IDs or a single numeric ID.
   *
   * @throws Exception
   */
  public function deleteMultiple(array $quantity_received_items) {
    $pom_quantity_received_ids = [];

    if (!empty($quantity_received_items)) {
      foreach ($quantity_received_items as $quantity_received) {
        try {
          // Invoke hook_entity_delete().
          module_invoke_all('entity_delete', $quantity_received, 'pom_quantity_received');
          field_attach_delete('pom_quantity_received', $quantity_received);
          $pom_quantity_received_ids[] = $quantity_received->pom_qty_received_id;

          db_delete('pom_quantity_received')
            ->condition('pom_qty_received_id', $pom_quantity_received_ids, 'IN')
            ->execute();
        }
        catch (Exception $e) {
          watchdog_exception('commerce_pom_quantity_received', $e);
          throw $e;
        }
      }
    }
  }

}
