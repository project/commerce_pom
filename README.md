CONTENTS OF THIS FILE
---------------------

 * About Commerce POM
 * The Commerce POM Architecture
 * Emails and PDF
 * Custom Settings and Configurations

About Commerce POM
------------------

Essentially, the Commerce Purchase Order Manager is a feature that allows a seller to create a list of products, along with their quantities, that they can send to vendors or vendors as a request for purchase. As the product lists are compiled, the module also understands the different quantity types that some products are packaged in, for example, some products might be ordered in cases or lots, where a single case might, in actuality, be 12 of the item. Commerce POM automatically calculates the number and sends the vendor the correct quantity. 

Furthermore, the module also provides an easy interface to track and receive the ordered products and promptly changes the stock field for each individual product as they are updated.

Commerce POM Dependencies
-------------------------

- Message
- Mimemail
- Mailsystem
- PDF Using mPDF (with the following patches):
  - https://www.drupal.org/node/2400245
  - https://www.drupal.org/node/2878929
  - https://www.drupal.org/node/2217889
- Commerce Simple Stock (commerce_stock)
- Commerce Extra: commerce_extra_quantity (>= 7.x-1.0-alpha1+16-dev)

Getting Started
---------------

- Enable the commerce_pom module
- Go to /admin/commerce/config/pom and configure the module
  - Ensure that you select a product and content display under 'PRODUCT OPTIONS'
    - This will add the necessary vendor-specific fields to the product types selected.
    - A brand and category vocabulary reference field must be provided for bulk adding of products to a purchase order to work.
- Ensure mailsystem module class used for Commerce POM Order module (commerce_pom_order) is MimemailSystem.


The Commerce POM Architecture
-----------------------------

* Purchase Order - a custom entity which stores the following fields:
  - Purchase Order Number
    - A field that is manually entered by the buyer when ordering products.
  - Invoice Number
    - The invoice number provided by the vendor when products are received.
  - Vendor Taxonomy Reference Field
    - This is a list of terms from the vendor vocabulary.
  - Purchase Order Manager Line Item Reference
    - Each purchase order can consist of multiple line items.
  - Vendor Email Override
    - This field will pre-populated from the vendor taxonomy reference field, however, can be overridden by the user for each purchase order.
  - Status
    - A select list, denoting the current status of the purchase order. It can be in any of the following statuses at any one time: Draft, Ordered, Partially Received, Completed, Completed with Discrepancies, Cancelled.
  - Date Created
    - When the purchase order was created.
  - Date Ordered
    - When the purchase order was sent to the vendor.

* Purchase Order Line Item - a custom entity. A purchase order can consist of many line items. A line item has the following fields:
  - Product Reference
    - The Commerce product reference field.
  - Order Quantity
    - The number ordered of a product.

* Quantity Received - a custom entity. This records the number of items received of a product at a particular date and time. This is needed as vendors might send the items in a purchase order in multiple shipments. A product might only be fulfilled in multiple shipments. So, this entity tracks how many are received each time. It consists of the following fields:
  - Quantity Received
  - Date Received
  - Purchase Order Line Item Entity Reference

* Vendor - a simple taxonomy vocabulary entity of all the vendors of the buyer. The vendor has a 1 to many relationship with a Purchase Order, so there can be many purchase orders, overtime, that are sent to the same vendor. Consists of the following fields:
  - Name
  - Email

* Purchase Order Manager Log Entry - a message entity. Every time a purchase order is updated, we automatically make a log of the changes. The buyer can also add manual log entries to a purchase order.


Emails and PDF
--------------

* Every time the buyer clicks to a send a purchase order, an email is sent to the vendor listing the items ordered along with a PDF attatchment, which can be printed by the vendor.


Custom Settings and Configurations
----------------------------------

* Lots Field - this field must be added to each product variation to denote the type of packaging that the product comes in. Some products are shipped in packages of 12, consequently, the lots quantity must be configured for them, so that, when a buyer adds that item to a purchase order, they can only be ordered in multiples of 12. Then, when we send the purchase order to the vendor, the quantity ordered would show as 1 (if 12 was ordered). For most products, the lot quantity would just be 1.

* Vendor Vocabulary - on installation of the Commerce POM module, a vendor taxonomy vocabulary will be created. However, the user can go to the settings page and change which vocabulary it should use, as perhaps, the site already has a vocabulary for the vendors.
