<?php

/**
 * @file
 * Purchase Order Manager View.
 */

$view = new view();
$view->name = 'commerce_pom_purchase_order_manager';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'pom_order';
$view->human_name = 'Commerce POM Purchase Order Manager';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Purchase Order Manager';
$handler->display->display_options['use_ajax'] = TRUE;
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['group_by'] = TRUE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'administer commerce purchase orders';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '10';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'pom_order_id' => 'pom_order_id',
  'purchase_order_id' => 'purchase_order_id',
  'pom_vendor' => 'pom_vendor',
  'ordered' => 'ordered',
  'pom_line_item_id' => 'pom_line_item_id',
  'status' => 'status',
  'nothing' => 'nothing',
  'nothing_1' => 'nothing',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'pom_order_id' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'purchase_order_id' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'pom_vendor' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'ordered' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'pom_line_item_id' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'status' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'nothing' => array(
    'align' => '',
    'separator' => '  ',
    'empty_column' => 0,
  ),
  'nothing_1' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* Header: Global: Text area */
$handler->display->display_options['header']['area_1']['id'] = 'area_1';
$handler->display->display_options['header']['area_1']['table'] = 'views';
$handler->display->display_options['header']['area_1']['field'] = 'area';
$handler->display->display_options['header']['area_1']['empty'] = TRUE;
$handler->display->display_options['header']['area_1']['content'] = 'Create a Purchase Order (P.O.) whenever ordering products from suppliers/vendors. Update orders as <strong>Invoice No.</strong> and <strong>Stock</strong> are received.';
$handler->display->display_options['header']['area_1']['format'] = 'filtered_html';
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['empty'] = TRUE;
$handler->display->display_options['empty']['area']['content'] = 'There are no purchase orders that match the search criteria.';
$handler->display->display_options['empty']['area']['format'] = 'filtered_html';
/* Relationship: Entity Reference: Referenced Entity */
$handler->display->display_options['relationships']['pom_line_item_target_id']['id'] = 'pom_line_item_target_id';
$handler->display->display_options['relationships']['pom_line_item_target_id']['table'] = 'field_data_pom_line_item';
$handler->display->display_options['relationships']['pom_line_item_target_id']['field'] = 'pom_line_item_target_id';
/* Relationship: Entity Reference: Referenced Entity */
$handler->display->display_options['relationships']['pom_product_category_target_id']['id'] = 'pom_product_category_target_id';
$handler->display->display_options['relationships']['pom_product_category_target_id']['table'] = 'field_data_pom_product_category';
$handler->display->display_options['relationships']['pom_product_category_target_id']['field'] = 'pom_product_category_target_id';
$handler->display->display_options['relationships']['pom_product_category_target_id']['relationship'] = 'pom_line_item_target_id';
/* Field: POM Order: POM Order ID */
$handler->display->display_options['fields']['pom_order_id']['id'] = 'pom_order_id';
$handler->display->display_options['fields']['pom_order_id']['table'] = 'pom_order';
$handler->display->display_options['fields']['pom_order_id']['field'] = 'pom_order_id';
$handler->display->display_options['fields']['pom_order_id']['exclude'] = TRUE;
/* Field: POM Order: POM Purchase Order ID */
$handler->display->display_options['fields']['purchase_order_id']['id'] = 'purchase_order_id';
$handler->display->display_options['fields']['purchase_order_id']['table'] = 'pom_order';
$handler->display->display_options['fields']['purchase_order_id']['field'] = 'purchase_order_id';
$handler->display->display_options['fields']['purchase_order_id']['label'] = 'P.O. #';
$handler->display->display_options['fields']['purchase_order_id']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['purchase_order_id']['alter']['make_link'] = TRUE;
$handler->display->display_options['fields']['purchase_order_id']['alter']['path'] = 'admin/commerce/purchase-orders/[pom_order_id]';
$handler->display->display_options['fields']['purchase_order_id']['alter']['target'] = '_blank';
/* Field: Commerce Purchase Order: POM Vendor */
$handler->display->display_options['fields']['pom_vendor']['id'] = 'pom_vendor';
$handler->display->display_options['fields']['pom_vendor']['table'] = 'field_data_pom_vendor';
$handler->display->display_options['fields']['pom_vendor']['field'] = 'pom_vendor';
$handler->display->display_options['fields']['pom_vendor']['label'] = 'Vendor';
$handler->display->display_options['fields']['pom_vendor']['type'] = 'taxonomy_term_reference_plain';
/* Field: POM Order: POM Ordered */
$handler->display->display_options['fields']['ordered']['id'] = 'ordered';
$handler->display->display_options['fields']['ordered']['table'] = 'pom_order';
$handler->display->display_options['fields']['ordered']['field'] = 'ordered';
$handler->display->display_options['fields']['ordered']['label'] = 'Date Ordered';
$handler->display->display_options['fields']['ordered']['empty'] = ' - ';
$handler->display->display_options['fields']['ordered']['date_format'] = 'custom';
$handler->display->display_options['fields']['ordered']['custom_date_format'] = 'F d, Y';
$handler->display->display_options['fields']['ordered']['second_date_format'] = 'long';
/* Field: COUNT(DISTINCT POM Line Item: POM Line Item ID) */
$handler->display->display_options['fields']['pom_line_item_id']['id'] = 'pom_line_item_id';
$handler->display->display_options['fields']['pom_line_item_id']['table'] = 'pom_line_item';
$handler->display->display_options['fields']['pom_line_item_id']['field'] = 'pom_line_item_id';
$handler->display->display_options['fields']['pom_line_item_id']['relationship'] = 'pom_line_item_target_id';
$handler->display->display_options['fields']['pom_line_item_id']['group_type'] = 'count_distinct';
$handler->display->display_options['fields']['pom_line_item_id']['label'] = 'No. Of Items';
/* Field: POM Order: POM Status */
$handler->display->display_options['fields']['status']['id'] = 'status';
$handler->display->display_options['fields']['status']['table'] = 'pom_order';
$handler->display->display_options['fields']['status']['field'] = 'status';
$handler->display->display_options['fields']['status']['label'] = 'Status';
/* Field: Global: Custom text */
$handler->display->display_options['fields']['nothing']['id'] = 'nothing';
$handler->display->display_options['fields']['nothing']['table'] = 'views';
$handler->display->display_options['fields']['nothing']['field'] = 'nothing';
$handler->display->display_options['fields']['nothing']['label'] = 'Operations';
$handler->display->display_options['fields']['nothing']['alter']['text'] = 'View';
$handler->display->display_options['fields']['nothing']['alter']['make_link'] = TRUE;
$handler->display->display_options['fields']['nothing']['alter']['path'] = 'admin/commerce/purchase-orders/[pom_order_id]';
$handler->display->display_options['fields']['nothing']['alter']['link_class'] = 'button';
$handler->display->display_options['fields']['nothing']['alter']['target'] = '_blank';
/* Field: vendor_order_pdf */
$handler->display->display_options['fields']['nothing_1']['id'] = 'nothing_1';
$handler->display->display_options['fields']['nothing_1']['table'] = 'views';
$handler->display->display_options['fields']['nothing_1']['field'] = 'nothing';
$handler->display->display_options['fields']['nothing_1']['ui_name'] = 'vendor_order_pdf';
$handler->display->display_options['fields']['nothing_1']['label'] = 'Order PDF';
$handler->display->display_options['fields']['nothing_1']['alter']['text'] = 'Vendor Order PDF';
$handler->display->display_options['fields']['nothing_1']['alter']['make_link'] = TRUE;
$handler->display->display_options['fields']['nothing_1']['alter']['path'] = 'admin/commerce/purchase-orders/[pom_order_id]/order-pdf';
$handler->display->display_options['fields']['nothing_1']['alter']['link_class'] = 'button';
$handler->display->display_options['fields']['nothing_1']['alter']['target'] = '_blank';
$handler->display->display_options['fields']['nothing_1']['element_label_colon'] = FALSE;
/* Sort criterion: POM Order: POM Created */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'pom_order';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Sort criterion: POM Order: POM Ordered */
$handler->display->display_options['sorts']['ordered']['id'] = 'ordered';
$handler->display->display_options['sorts']['ordered']['table'] = 'pom_order';
$handler->display->display_options['sorts']['ordered']['field'] = 'ordered';
$handler->display->display_options['sorts']['ordered']['order'] = 'DESC';
/* Filter criterion: Commerce Purchase Order: POM Vendor (pom_vendor) */
$handler->display->display_options['filters']['pom_vendor_tid']['id'] = 'pom_vendor_tid';
$handler->display->display_options['filters']['pom_vendor_tid']['table'] = 'field_data_pom_vendor';
$handler->display->display_options['filters']['pom_vendor_tid']['field'] = 'pom_vendor_tid';
$handler->display->display_options['filters']['pom_vendor_tid']['group'] = 1;
$handler->display->display_options['filters']['pom_vendor_tid']['exposed'] = TRUE;
$handler->display->display_options['filters']['pom_vendor_tid']['expose']['operator_id'] = 'pom_vendor_tid_op';
$handler->display->display_options['filters']['pom_vendor_tid']['expose']['label'] = 'Vendor';
$handler->display->display_options['filters']['pom_vendor_tid']['expose']['operator'] = 'pom_vendor_tid_op';
$handler->display->display_options['filters']['pom_vendor_tid']['expose']['identifier'] = 'pom_vendor_tid';
$handler->display->display_options['filters']['pom_vendor_tid']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
);
$handler->display->display_options['filters']['pom_vendor_tid']['type'] = 'select';
$handler->display->display_options['filters']['pom_vendor_tid']['vocabulary'] = 'pom_vendor_vocab';
/* Filter criterion: Taxonomy term: Name */
$handler->display->display_options['filters']['name']['id'] = 'name';
$handler->display->display_options['filters']['name']['table'] = 'taxonomy_term_data';
$handler->display->display_options['filters']['name']['field'] = 'name';
$handler->display->display_options['filters']['name']['relationship'] = 'pom_product_category_target_id';
$handler->display->display_options['filters']['name']['group'] = 1;
$handler->display->display_options['filters']['name']['exposed'] = TRUE;
$handler->display->display_options['filters']['name']['expose']['operator_id'] = 'name_op';
$handler->display->display_options['filters']['name']['expose']['label'] = 'Product Category';
$handler->display->display_options['filters']['name']['expose']['operator'] = 'name_op';
$handler->display->display_options['filters']['name']['expose']['identifier'] = 'product_category';
$handler->display->display_options['filters']['name']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
);
/* Filter criterion: POM Order: POM Status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'pom_order';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['exposed'] = TRUE;
$handler->display->display_options['filters']['status']['expose']['operator_id'] = 'status_op';
$handler->display->display_options['filters']['status']['expose']['label'] = 'Status';
$handler->display->display_options['filters']['status']['expose']['operator'] = 'status_op';
$handler->display->display_options['filters']['status']['expose']['identifier'] = 'status';
$handler->display->display_options['filters']['status']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
);
/* Filter criterion: POM Order: POM Ordered */
$handler->display->display_options['filters']['ordered_1']['id'] = 'ordered_1';
$handler->display->display_options['filters']['ordered_1']['table'] = 'pom_order';
$handler->display->display_options['filters']['ordered_1']['field'] = 'ordered';
$handler->display->display_options['filters']['ordered_1']['operator'] = '>=';
$handler->display->display_options['filters']['ordered_1']['group'] = 1;
$handler->display->display_options['filters']['ordered_1']['exposed'] = TRUE;
$handler->display->display_options['filters']['ordered_1']['expose']['operator_id'] = 'ordered_1_op';
$handler->display->display_options['filters']['ordered_1']['expose']['label'] = 'Date - Start';
$handler->display->display_options['filters']['ordered_1']['expose']['operator'] = 'ordered_1_op';
$handler->display->display_options['filters']['ordered_1']['expose']['identifier'] = 'ordered_1';
$handler->display->display_options['filters']['ordered_1']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
);
/* Filter criterion: POM Order: POM Ordered */
$handler->display->display_options['filters']['ordered_2']['id'] = 'ordered_2';
$handler->display->display_options['filters']['ordered_2']['table'] = 'pom_order';
$handler->display->display_options['filters']['ordered_2']['field'] = 'ordered';
$handler->display->display_options['filters']['ordered_2']['operator'] = '<=';
$handler->display->display_options['filters']['ordered_2']['group'] = 1;
$handler->display->display_options['filters']['ordered_2']['exposed'] = TRUE;
$handler->display->display_options['filters']['ordered_2']['expose']['operator_id'] = 'ordered_2_op';
$handler->display->display_options['filters']['ordered_2']['expose']['label'] = 'Date - End';
$handler->display->display_options['filters']['ordered_2']['expose']['operator'] = 'ordered_2_op';
$handler->display->display_options['filters']['ordered_2']['expose']['identifier'] = 'ordered_2';
$handler->display->display_options['filters']['ordered_2']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
);
/* Filter criterion: POM Order: POM Purchase Order ID */
$handler->display->display_options['filters']['purchase_order_id']['id'] = 'purchase_order_id';
$handler->display->display_options['filters']['purchase_order_id']['table'] = 'pom_order';
$handler->display->display_options['filters']['purchase_order_id']['field'] = 'purchase_order_id';
$handler->display->display_options['filters']['purchase_order_id']['group'] = 1;
$handler->display->display_options['filters']['purchase_order_id']['exposed'] = TRUE;
$handler->display->display_options['filters']['purchase_order_id']['expose']['operator_id'] = 'purchase_order_id_op';
$handler->display->display_options['filters']['purchase_order_id']['expose']['label'] = 'Number';
$handler->display->display_options['filters']['purchase_order_id']['expose']['operator'] = 'purchase_order_id_op';
$handler->display->display_options['filters']['purchase_order_id']['expose']['identifier'] = 'purchase_order_id';
$handler->display->display_options['filters']['purchase_order_id']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'admin/commerce/purchase-orders';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'Purchase Orders';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
