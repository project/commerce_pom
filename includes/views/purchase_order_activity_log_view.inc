<?php

/**
 * @file
 * Purchase Order Activity Log View.
 */

$view = new view();
$view->name = 'commerce_pom_purchase_order_activity_log';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'message';
$view->human_name = 'Commerce POM Purchase Order Activity Log';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Activity Log';
$handler->display->display_options['use_ajax'] = TRUE;
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['group_by'] = TRUE;
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['empty'] = TRUE;
$handler->display->display_options['empty']['area']['content'] = 'There are no logs for this purchase order.';
$handler->display->display_options['empty']['area']['format'] = 'filtered_html';
/* Relationship: Message: User uid */
$handler->display->display_options['relationships']['user']['id'] = 'user';
$handler->display->display_options['relationships']['user']['table'] = 'message';
$handler->display->display_options['relationships']['user']['field'] = 'user';
/* Field: Message: Message ID */
$handler->display->display_options['fields']['mid']['id'] = 'mid';
$handler->display->display_options['fields']['mid']['table'] = 'message';
$handler->display->display_options['fields']['mid']['field'] = 'mid';
$handler->display->display_options['fields']['mid']['exclude'] = TRUE;
/* Field: Message: Timestamp */
$handler->display->display_options['fields']['timestamp']['id'] = 'timestamp';
$handler->display->display_options['fields']['timestamp']['table'] = 'message';
$handler->display->display_options['fields']['timestamp']['field'] = 'timestamp';
$handler->display->display_options['fields']['timestamp']['label'] = '';
$handler->display->display_options['fields']['timestamp']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['timestamp']['alter']['text'] = '[timestamp]:';
$handler->display->display_options['fields']['timestamp']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['timestamp']['date_format'] = 'custom';
$handler->display->display_options['fields']['timestamp']['custom_date_format'] = 'm-d-y';
$handler->display->display_options['fields']['timestamp']['second_date_format'] = 'long';
/* Field: User: Name */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'users';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['relationship'] = 'user';
$handler->display->display_options['fields']['name']['label'] = '';
$handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
/* Field: Message: Activity Summary */
$handler->display->display_options['fields']['pom_activity_summary']['id'] = 'pom_activity_summary';
$handler->display->display_options['fields']['pom_activity_summary']['table'] = 'field_data_pom_activity_summary';
$handler->display->display_options['fields']['pom_activity_summary']['field'] = 'pom_activity_summary';
$handler->display->display_options['fields']['pom_activity_summary']['label'] = '';
$handler->display->display_options['fields']['pom_activity_summary']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['pom_activity_summary']['group_column'] = 'entity_id';
/* Sort criterion: Message: Timestamp */
$handler->display->display_options['sorts']['timestamp']['id'] = 'timestamp';
$handler->display->display_options['sorts']['timestamp']['table'] = 'message';
$handler->display->display_options['sorts']['timestamp']['field'] = 'timestamp';
$handler->display->display_options['sorts']['timestamp']['order'] = 'DESC';
/* Contextual filter: Message: Purchase Order (pom_purchase_order) */
$handler->display->display_options['arguments']['pom_purchase_order_target_id']['id'] = 'pom_purchase_order_target_id';
$handler->display->display_options['arguments']['pom_purchase_order_target_id']['table'] = 'field_data_pom_purchase_order';
$handler->display->display_options['arguments']['pom_purchase_order_target_id']['field'] = 'pom_purchase_order_target_id';
$handler->display->display_options['arguments']['pom_purchase_order_target_id']['default_action'] = 'default';
$handler->display->display_options['arguments']['pom_purchase_order_target_id']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['pom_purchase_order_target_id']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['pom_purchase_order_target_id']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['pom_purchase_order_target_id']['summary_options']['items_per_page'] = '25';
