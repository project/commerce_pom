<?php

/**
 * @file
 * Administrative callbacks and form builder functions for Commerce POM.
 */

/**
 * Commerce POM admin form.
 */
function commerce_pom_admin_form($form, &$form_state) {
  $form['#tree'] = TRUE;

  // The necessary product options.
  $form['product_options'] = [
    '#type' => 'fieldset',
    '#title' => t('Product Options'),
    '#description' => t('Choose products that purchase orders will work with. Fields that help manage vendor-specific information will be added to these types of products. You must <strong>choose at least one product type</strong> or the module will not work.'),
  ];

  // Select product types, basically this will add the lot quantity and vendor
  // SKU fields to all product variations in the selected product types.
  $form['product_options']['products'] = [
    '#type' => 'fieldset',
    '#title' => t('Products'),
    '#description' => t('Products that can be added to purchase orders.'),
  ];

  $form_state['commerce_product_types'] = commerce_product_types();
  // Create a checkbox for each product type, set with the current lot quantity
  // and vendor SKU enabled state.
  foreach ($form_state['commerce_product_types'] as $type => $product_type) {
    // We'll just use the lot_quantity to check if already enabled as the vendor
    // SKU and lot quantity are both enabled and disabled together.
    $instance[$type] = field_info_instance('commerce_product', 'pom_lot_quantity', $type);
    $enabled[$type] = (!empty($instance[$type]));

    $form['product_options']['products'][$type] = [
      '#type' => 'checkbox',
      '#default_value' => $enabled[$type],
      '#title' => t('@name', [
        '@name' => $product_type['name'],
        '@machine_name' => $type,
      ]),
    ];
  }

  // Add a checkbox that requires them to say "I do", but don't show it
  // (#access == FALSE) unless they're deleting.
  if (!empty($form_state['products']['delete_instances'])) {
    $type_plural = format_plural(count($form_state['products']['delete_instances']), t('type'), t('types'));
    $affirmation = t('I understand that all lot quantity and vendor SKU data will be permanently removed from the product @type_plural %product_types.',
      [
        '@type_plural' => $type_plural,
        '%product_types' => implode(', ', $form_state['products']['delete_instances']),
      ]
    );
  }

  // Our bulk product add configuration fields.
  $form['product_options']['bulk_add_products'] = [
    '#type' => 'fieldset',
    '#title' => t('Content Displays'),
    '#description' => t('Types of content pages that are used to display the above products. This is used for finding categories related to bulk adding products.'),
    '#prefix' => '<div id="pom-bulk-add-products-config">',
    '#suffix' => '</div>',
  ];

  // Make the product display bundles configurable.
  // First, let's output the product display bundles.
  $options = [];
  $form_state['field_instances'] = [];
  // The product display bundles should have a commerce product reference field.
  foreach (node_type_get_types() as $type => $node) {
    $form_state['field_instances'][$type] = field_info_instances('node', $type);

    $has_commerce_product_reference_field = FALSE;
    foreach ($form_state['field_instances'][$type] as $instance) {
      $field = field_info_field($instance['field_name']);
      if ($field['type'] == 'commerce_product_reference') {
        $has_commerce_product_reference_field = TRUE;
      }
    }

    // If this bundle has a product reference field add it to our options.
    if ($has_commerce_product_reference_field) {
      $options[$type] = $node->name;
    }
  }

  $form['product_options']['bulk_add_products']['commerce_pom_product_display_bundles'] = [
    '#type' => 'checkboxes',
    '#title' => t('Product Display Bundles'),
    '#options' => $options,
    '#description' => t('Select the product display bundles to use for the Purchase Order Manager bulk add products search feature.'),
    '#default_value' => !empty(variable_get('commerce_pom_product_display_bundles', '')) ? variable_get('commerce_pom_product_display_bundles', '') : [],
    '#ajax' => [
      'callback' => 'commerce_pom_bulk_add_products_config_ajax_callback',
      'wrapper' => 'pom-bulk-add-products-config',
    ],
    '#required' => TRUE,
  ];

  // Make the brand and category fields configurable. The fields that appear
  // for the brand and category will be dependable on the product display.
  $fields = [];
  $bundle_types = [];
  if (isset($form_state['values']['product_options']['bulk_add_products']['commerce_pom_product_display_bundles'])) {
    $bundle_types = $form_state['values']['product_options']['bulk_add_products']['commerce_pom_product_display_bundles'];
  }
  elseif (!empty(variable_get('commerce_pom_product_display_bundles', ''))) {
    $bundle_types = variable_get('commerce_pom_product_display_bundles', '');
  }
  foreach ($bundle_types as $node_type) {
    if ($node_type) {
      $instances = $form_state['field_instances'][$node_type];
      foreach ($instances as $key => $instance) {
        $fields[$key] = $instance['field_name'];
      }
    }
  }

  // Make the brand field configurable.
  $form['product_options']['bulk_add_products']['commerce_pom_brand_field'] = [
    '#type' => 'select',
    '#title' => t('Brand Field Reference'),
    '#options' => $fields,
    '#description' => t('Select the brand field reference to use for the Purchase Order Manager bulk add products search feature.'),
    '#default_value' => variable_get('commerce_pom_brand_field', ''),
    '#states' => [
      'visible' => [
        [
          [':input[name="product_options[bulk_add_products][commerce_pom_product_display_bundles][article]"]' => ['checked' => TRUE]],
          'or',
          [':input[name="product_options[bulk_add_products][commerce_pom_product_display_bundles][page]"]' => ['checked' => TRUE]],
          'or',
          [':input[name="product_options[bulk_add_products][commerce_pom_product_display_bundles][product_display]"]' => ['checked' => TRUE]],
        ],
      ],
    ],
  ];

  // Make the category field configurable.
  $form['product_options']['bulk_add_products']['commerce_pom_category_field'] = [
    '#type' => 'select',
    '#title' => t('Category Field Reference'),
    '#options' => $fields,
    '#description' => t('Select the category field reference to use for the Purchase Order Manager bulk add products search feature.'),
    '#default_value' => variable_get('commerce_pom_category_field', ''),
    '#states' => [
      'visible' => [
        [
          [':input[name="product_options[bulk_add_products][commerce_pom_product_display_bundles][article]"]' => ['checked' => TRUE]],
          'or',
          [':input[name="product_options[bulk_add_products][commerce_pom_product_display_bundles][page]"]' => ['checked' => TRUE]],
          'or',
          [':input[name="product_options[bulk_add_products][commerce_pom_product_display_bundles][product_display]"]' => ['checked' => TRUE]],
        ],
      ],
    ],
  ];

  // Configuration for the email/PDF view.
  $form['commerce_pom_email_pdf_config'] = [
    '#type' => 'fieldset',
    '#title' => t('Purchase Order Email/PDF Configuration'),
  ];

  // Custom stylesheet to use for PDF.
  $form['commerce_pom_email_pdf_config']['commerce_pom_pdf_using_mpdf_pdf_css_file'] = [
    '#type' => 'textfield',
    '#title' => t('Custom Stylsheet for PDF files'),
    '#description' => t('Enter the directory and name of your custom stylesheet. ie. "sites/all/modules/my_custom_module/css/my_custom_style.css". If left blank, a generic style from this module will be used.'),
    '#default_value' => variable_get('commerce_pom_pdf_using_mpdf_pdf_css_file', ''),
  ];

  // Set custom logo for all purchase order emails/PDFs.
  $form['commerce_pom_email_pdf_config']['commerce_pom_po_site_logo'] = [
    '#type' => 'managed_file',
    '#title' => t('Logo for Emails and PDFs'),
    '#description' => t('Add the image that will be used as the site logo for all purchase order emails and PDFs.'),
    '#default_value' => variable_get('commerce_pom_po_site_logo', NULL),
    '#upload_location' => 'public://commerce_pom_default_images',
    '#upload_validators' => [
      'file_validate_extensions' => ['png jpg jpeg'],
      'file_validate_image_resolution' => ['200x200', 0],
    ],
  ];

  // Set custom contact text for all purchase order emails/PDFs.
  $form['commerce_pom_email_pdf_config']['commerce_pom_po_contact_text'] = [
    '#type' => 'textarea',
    '#title' => t('Email/PDF Footer'),
    '#description' => t('Enter the contact text that should go at the bottom of all purchase order emails and PDFs. HTML is allowed.'),
    '#default_value' => variable_get('commerce_pom_po_contact_text', t('Please contact us if you have any questions regarding this order.<p>!email<p>@site_name', [
      '!email' => l(variable_get('site_mail', ''), 'mailto:' . variable_get('site_mail', '')),
      '@site_name' => variable_get('site_name', ''),
    ])),
  ];

  // BCC Email Address.
  $form['commerce_pom_email_pdf_config']['commerce_pom_email_bcc_address'] = [
    '#type' => 'textfield',
    '#title' => t('PO BCC Email Address'),
    '#description' => t('Enter the BCC email address that will be notified when purchase orders are sent. You can add multiple emails separated by a comma and a space.'),
    '#default_value' => variable_get('commerce_pom_email_bcc_address', variable_get('site_mail', '')),
  ];

  // Configure the image fields for each product type.
  $form['field_settings'] = [
    '#type' => 'fieldset',
    '#title' => t('Select Product Image Field'),
  ];
  $options = [NULL => t('No field')];
  foreach (field_info_fields() as $field_name => $field) {
    if ($field['type'] == 'image') {
      $options[$field_name] = $field_name;
    }
  }

  foreach ($form_state['commerce_product_types'] as $name => $type) {
    $variable_name = 'commerce_pom_image_field_' . $name;

    $form['field_settings'][$name] = [
      '#type' => 'fieldset',
      '#title' => $type['name'],
      '#states' => [
        'invisible' => [
          ':input[name="commerce_pom_available_products[' . $name . ']"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['field_settings'][$name][$variable_name] = [
      '#type' => 'select',
      '#title' => t('Image field(s)'),
      '#description' => t('Select the field that you use to store images of your products in order to show thumbnails in search results.'),
      '#options' => $options,
      '#default_value' => variable_get($variable_name, NULL),
      '#multiple' => TRUE,
    ];

    $variable_name = 'commerce_pom_image_default_' . $name;

    $form['field_settings'][$name][$variable_name] = [
      '#type' => 'managed_file',
      '#title' => t('Default image'),
      '#description' => t('You may provide a default image for when no other image is available.'),
      '#default_value' => variable_get($variable_name, NULL),
      '#upload_location' => 'public://commerce_pom_default_images',
      '#upload_validators' => [
        'file_validate_is_image' => [],
        'file_validate_extensions' => ['png gif jpg jpeg'],
      ],
    ];
  }

  // Advanced settings.
  $form['commerce_pom_advanced_config'] = [
    '#type' => 'fieldset',
    '#title' => t('Advanced'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
  ];

  // Make the vendor vocabulary configurable.
  // First get all the vocabularies in the system so we can add them to our
  // select list options.
  $options = [];
  $vocabularies = db_query('SELECT machine_name, name FROM {taxonomy_vocabulary} ORDER BY name');
  foreach ($vocabularies as $vocabulary) {
    $options[$vocabulary->machine_name] = $vocabulary->name;
  }
  $form['commerce_pom_advanced_config']['commerce_pom_vendor_vocabulary'] = [
    '#type' => 'select',
    '#title' => t('Vendor Information'),
    '#required' => TRUE,
    '#options' => $options,
    '#description' => t('Choose a term list that will represent your Vendors. Additional field for an email will be automatically added. Existing bundles can be used. The bundle initially selected comes default with Purchase Order Manager.'),
    '#default_value' => variable_get('commerce_pom_vendor_vocabulary', 'pom_vendor_vocab'),
  ];

  // Configure the search results count.
  $form['commerce_pom_advanced_config']['commerce_pom_search_results_count'] = [
    '#type' => 'textfield',
    '#title' => t('Search Results Count'),
    '#required' => TRUE,
    '#description' => t('Set the number of results to show in the POM product autocomplete.'),
    '#default_value' => variable_get('commerce_pom_search_results_count', 5),
    '#element_validate' => ['element_validate_integer_positive'],
  ];

  // Configure the search api index for POM.
  if (module_exists('search_api')) {
    $index_options = [
      'default' => t('Use default search'),
    ];
    foreach (search_api_index_load_multiple(FALSE) as $id => $index) {
      $index_options[$id] = $index->name;
    }

    $form['commerce_pom_advanced_config']['commerce_pom_search_api_index'] = [
      '#type' => 'select',
      '#title' => t('Search API Index'),
      '#description' => t('If you would like to use a search API index when searching for products in the POM, you may select one here.'),
      '#options' => $index_options,
      '#default_value' => variable_get('commerce_pom_search_api_index', NULL),
    ];
  }

  $form['confirmation'] = [
    '#type' => 'checkbox',
    '#title' => !empty($affirmation) ? $affirmation : '',
    '#default_value' => FALSE,
    '#access' => FALSE,
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Apply Changes'),
  ];

  // If they're deleting the lot quantity, show the confirmation checkbox.
  if (!empty($form_state['products']['delete_instances'])) {
    $form['confirmation']['#access'] = TRUE;
    drupal_set_message(t('You must click the confirmation checkbox to confirm that you want to delete lot quantity and vendor SKU data.'), 'warning');
  }

  return $form;
}

/**
 * Form validate handler for the commerce_pom admin form.
 */
function commerce_pom_admin_form_validate($form, &$form_state) {
  // If they are deleting the lot quantity and have not checked the confirmation
  // checkbox, make them do so.
  if (!empty($form_state['products']['delete_instances']) && empty($form_state['values']['confirmation'])) {
    form_set_error('confirmation', t('Please check the "I understand" checkbox to indicate you understand that all lot quantity and vendor SKU data in these fields will be deleted: %fields.', ['%fields' => implode(', ', $form_state['products']['delete_instances'])]));
  }

  // Make sure at least one product type is selected.
  $products_selected = FALSE;
  foreach ($form_state['values']['product_options']['products'] as $type => $enable) {
    if ($enable) {
      $products_selected = TRUE;
      break;
    }
  }
  if (!$products_selected) {
    drupal_set_message(t('At least one product type should be selected that can be added to purchase orders before the module will work.'), 'warning');
  }

  // Validate BCC email address.
  $email_addresses = explode(', ', $form_state['values']['commerce_pom_email_pdf_config']['commerce_pom_email_bcc_address']);
  $email_valid = TRUE;
  foreach ($email_addresses as $email) {
    if (!valid_email_address($email)) {
      $email_valid = FALSE;
      break;
    }
  }
  if (!$email_valid) {
    form_set_error('commerce_pom_email_pdf_config][commerce_pom_email_bcc_address', t('Please enter a valid email address.'));
  }

  // Setup for image fields.
  $directory = 'public://commerce_pom_default_thumbnails';
  file_prepare_directory($directory, FILE_MODIFY_PERMISSIONS | FILE_CREATE_DIRECTORY);
}

/**
 * Add or remove the lot quantity field from product types.
 */
function commerce_pom_admin_form_submit($form, &$form_state) {
  $form_state['products']['delete_instances'] = [];

  // Prepare a batch in case we need it for enabling lot quantity on product
  // types.
  $batch = [
    // These are set below if needed.
    'operations' => [],
    'file' => drupal_get_path('module', 'commerce_pom') . '/includes/commerce_pom.admin.inc',
    'finished' => 'commerce_pom_batch_product_type_init_finished',
    'title' => t('Product lot quantity initialization'),
    'init_message' => t('Product lot quantity initialization is starting.'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Product lot quantity initialization has encountered an error.'),
  ];

  foreach ($form_state['values']['product_options']['products'] as $type => $enable) {
    $instance = field_info_instance('commerce_product', 'pom_lot_quantity', $type);

    $currently_enabled = commerce_pom_product_type_enabled($type);
    // If they want us to enable it and it doesn't currently exist, do the work.
    if ($enable && !$currently_enabled) {
      // Create the instance.
      commerce_pom_admin_create_instance('pom_lot_quantity', 'number_decimal', TRUE, 'commerce_product', $type, t('Lot Quantity'), t('Used with the P.O. Manager. Example: Set this Quantity to 12 if you order 1 case of hats from your vendor, but you receive 12 hats into your inventory. Leave as default (1) if you order 1 and receive 1.'));
      drupal_set_message(t('Lot quantity field has been added to the %type product type.', ['%type' => $type]));
      // Add the operation to process this type to the batch.
      $batch['operations'][] = [
        'commerce_pom_batch_product_type_init_process',
        [$type, 1, 'pom_lot_quantity'],
      ];
    }
    // Conversely, if they *don't* want it and it's currently enabled,
    // warn them about the consequences or do it.
    else {
      if (!$enable && $currently_enabled) {
        // If they haven't clicked the "confirm" checkbox, rebuild and get them
        // to do it.
        if (empty($form_state['values']['confirmation'])) {
          $form_state['products']['delete_instances'][] = $type;
          $form_state['rebuild'] = TRUE;
        }
        // Otherwise they already have clicked it and we can delete.
        else {
          // Remove the instance.
          field_delete_instance($instance);

          drupal_set_message(t('Lot quantity has been disabled on the %type product type', ['%type' => $type]));
        }
      }
    }
  }

  // If our batch has operations, run it now.
  if (count($batch['operations'])) {
    batch_set($batch);
  }

  // If the user changed the vendor vocabulary set it in our variable and
  // update the pom_vendor field to use the new vocabulary.
  if (variable_get('commerce_pom_vendor_vocabulary', 'pom_vendor_vocab') != $form_state['values']['commerce_pom_advanced_config']['commerce_pom_vendor_vocabulary']) {
    // Fetch our vendor field from the purchase order entity and update it.
    $info = field_info_field('pom_vendor');
    $values = &$info['settings']['allowed_values'];
    $values[0]['vocabulary'] = $form_state['values']['commerce_pom_advanced_config']['commerce_pom_vendor_vocabulary'];
    field_update_field($info);


    // Add an email field to the newly selected vocab.
    $field_instance = field_info_instance('taxonomy_term', 'pom_vendor_email', $form_state['values']['commerce_pom_advanced_config']['commerce_pom_vendor_vocabulary']);
    if (!$field_instance) {
      $field = 'pom_vendor_email';
      // Attach the field to our vendor taxonomy entity.
      $instance = [
        'field_name' => 'pom_vendor_email',
        'entity_type' => 'taxonomy_term',
        'bundle' => $form_state['values']['commerce_pom_advanced_config']['commerce_pom_vendor_vocabulary'],
        'label' => t('Vendor Email'),
        'description' => t('Enter the vendor email address.'),
        'required' => TRUE,
        'widget' => [
          'type' => 'email_textfield',
          'weight' => 0,
        ],
      ];
      field_create_instance($instance);

      // Add a message saying they need to add the email for these terms in the
      // newly selected vocab.
      drupal_set_message(t('The vendor vocabulary has been successfully changed and a new email field has been added to the @vocab vocabulary entity.
      Please ensure you add the associated email to the terms in this vocabulary.', [
        '@vocab' => $form_state['values']['commerce_pom_advanced_config']['commerce_pom_vendor_vocabulary'],
      ]));
    }

    // Now, set the new value in the vendor vocabulary variable.
    variable_set('commerce_pom_vendor_vocabulary', $form_state['values']['commerce_pom_advanced_config']['commerce_pom_vendor_vocabulary']);
  }

  // Set the search results count.
  variable_set('commerce_pom_search_results_count', $form_state['values']['commerce_pom_advanced_config']['commerce_pom_search_results_count']);

  // Set the search api index for POM.
  if (!empty($form_state['values']['commerce_pom_advanced_config']['commerce_pom_search_api_index'])) {
    variable_set('commerce_pom_search_api_index', $form_state['values']['commerce_pom_advanced_config']['commerce_pom_search_api_index']);
  }

  // Set the PO site logo image.
  $image_variable = 'commerce_pom_po_site_logo';
  $existing_image_fid = variable_get($image_variable, 0);
  $remove_existing = FALSE;
  $new_fid = $existing_image_fid;

  if (!empty($form_state['values']['commerce_pom_email_pdf_config'][$image_variable])) {
    if ($form_state['values']['commerce_pom_email_pdf_config'][$image_variable] != $existing_image_fid) {
      $file = file_load($form_state['values']['commerce_pom_email_pdf_config'][$image_variable]);
      $file->status = FILE_STATUS_PERMANENT;

      file_save($file);
      file_usage_add($file, 'commerce_pom', 'settings', 1);

      $new_fid = $file->fid;
      $remove_existing = TRUE;
    }
  }
  else {
    $new_fid = 0;
    $remove_existing = TRUE;
  }
  if ($existing_image_fid && $remove_existing) {
    if ($file = file_load($existing_image_fid)) {
      file_delete($file, TRUE);
    }
  }
  variable_set($image_variable, $new_fid);

  // Set the commerce product types image fields.
  foreach ($form_state['commerce_product_types'] as $name => $type) {
    variable_set('commerce_pom_image_field_' . $name, $form_state['values']['field_settings'][$name]['commerce_pom_image_field_' . $name]);

    $image_variable = 'commerce_pom_image_default_' . $name;
    $existing_image_fid = variable_get($image_variable, 0);
    $remove_existing = FALSE;
    $new_fid = $existing_image_fid;

    if (!empty($form_state['values']['field_settings'][$name][$image_variable])) {
      if ($form_state['values']['field_settings'][$name][$image_variable] != $existing_image_fid) {
        $file = file_load($form_state['values']['field_settings'][$name][$image_variable]);
        $file->status = FILE_STATUS_PERMANENT;

        file_save($file);
        file_usage_add($file, 'commerce_pom', 'settings', 1);

        $new_fid = $file->fid;
        $remove_existing = TRUE;
      }
    }
    else {
      $new_fid = 0;
      $remove_existing = TRUE;
    }

    if ($existing_image_fid && $remove_existing) {
      if ($file = file_load($existing_image_fid)) {
        file_delete($file, TRUE);
      }
    }

    variable_set($image_variable, $new_fid);
  }

  if (!empty($form_state['storage']['files'])) {
    foreach ($form_state['storage']['files'] as $variable_name => $file) {
      variable_set($variable_name, $file->fid);
    }
  }

  // Now, set the new value in the products display variable.
  variable_set('commerce_pom_product_display_bundles', $form_state['values']['product_options']['bulk_add_products']['commerce_pom_product_display_bundles']);

  // Now, set the new value in the brand vocabulary variable.
  variable_set('commerce_pom_brand_field', $form_state['values']['product_options']['bulk_add_products']['commerce_pom_brand_field']);

  // Now, set the new value in the category vocabulary variable.
  variable_set('commerce_pom_category_field', $form_state['values']['product_options']['bulk_add_products']['commerce_pom_category_field']);

  // Set the custom PDF stylesheet.
  variable_set('commerce_pom_pdf_using_mpdf_pdf_css_file', $form_state['values']['commerce_pom_email_pdf_config']['commerce_pom_pdf_using_mpdf_pdf_css_file']);

  // Set the custom PO contact text.
  variable_set('commerce_pom_po_contact_text', $form_state['values']['commerce_pom_email_pdf_config']['commerce_pom_po_contact_text']);

  // Set the BCC email address.
  variable_set('commerce_pom_email_bcc_address', $form_state['values']['commerce_pom_email_pdf_config']['commerce_pom_email_bcc_address']);

  // Prepare a batch in case we need it for enabling vendor sku on product
  // types.
  $vendor_sku_batch = [
    // These are set below if needed.
    'operations' => [],
    'file' => drupal_get_path('module', 'commerce_pom') . '/includes/commerce_pom.admin.inc',
    'finished' => 'commerce_pom_batch_product_type_init_finished',
    'title' => t('Product vendor sku field initialization'),
    'init_message' => t('Product vendor sku field initialization is starting.'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Product vendor sku field initialization has encountered an error.'),
  ];

  foreach ($form_state['values']['product_options']['products'] as $type => $enable) {
    $instance = field_info_instance('commerce_product', 'pom_vendor_sku', $type);

    $currently_enabled = commerce_pom_product_type_vendor_sku_enabled($type);
    // If they want us to enable it and it doesn't currently exist, do the work.
    if ($enable && !$currently_enabled) {
      // Create the instance.
      commerce_pom_admin_create_instance('pom_vendor_sku', 'text', FALSE, 'commerce_product', $type, t('Vendor SKU'), t('Used with the P.O. Manager. This is the vendor/supplier SKU for the product.'));
      drupal_set_message(t('Vendor SKU field has been added to the %type product type.', ['%type' => $type]));
      // Add the operation to process this type to the batch.
      $vendor_sku_batch['operations'][] = [
        'commerce_pom_batch_product_type_init_process',
        [$type, '', 'pom_vendor_sku'],
      ];
    }
    // Conversely, if they *don't* want it and it's currently enabled,
    // warn them about the consequences or do it.
    else {
      if (!$enable && $currently_enabled) {
        // If they haven't clicked the "confirm" checkbox, rebuild and get them
        // to do it.
        if (empty($form_state['values']['confirmation'])) {
          $form_state['products']['delete_instances'][] = $type;
          $form_state['rebuild'] = TRUE;
        }
        // Otherwise they already have clicked it and we can delete.
        else {
          // Remove the instance.
          field_delete_instance($instance);

          drupal_set_message(t('Vendor SKU field has been disabled on the %type product type', ['%type' => $type]));
        }
      }
    }
  }

  // If our batch has operations, run it now.
  if (count($vendor_sku_batch['operations'])) {
    batch_set($vendor_sku_batch);
  }

  drupal_set_message(t('Settings for the purchase order manager have been saved.'));
}

/**
 * Batch Operation Callback.
 *
 * @param string $type
 *   The product type to process.
 * @param int $init_value
 *   The initial value to give to the lot quantity/vendor sku field.
 * @param string $field_name
 *   The name of the field we're creating. Defaults to pom_lot_quantity.
 * @param array $context
 *   The context of the batch product type.
 */
function commerce_pom_batch_product_type_init_process($type, $init_value, $field_name = 'pom_lot_quantity', array &$context) {
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;

    // Get the products to operate on.
    $result = db_query("SELECT product_id FROM {commerce_product} cp WHERE cp.type = :type", [
      ':type' => $type,
    ]);
    $context['sandbox']['product_data'] = $result->fetchAll();
    $context['sandbox']['max'] = count($context['sandbox']['product_data']);
  }

  // We can safely process 10 at a time without a timeout.
  $limit = 25;
  $current_count = 0;

  while ($current_count < $limit && count($context['sandbox']['product_data'])) {
    // Update our counts.
    $current_count++;
    $context['sandbox']['progress']++;

    // Load the product and get its wrapper.
    $product_data = array_shift($context['sandbox']['product_data']);
    $product = commerce_product_load($product_data->product_id);

    $wrapper = entity_metadata_wrapper('commerce_product', $product);
    $wrapper->$field_name = $init_value;
    $wrapper->save();

    $context['message'] = t('Processing product type %type %progress% complete.', [
      '%type' => $type,
      '%progress' => intval($context['sandbox']['progress'] / $context['sandbox']['max'] * 100),
    ]);
  } // product processing loop

  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] >= $context['sandbox']['max'];
  }
}

/**
 * Batch 'finished' callback.
 */
function commerce_pom_batch_product_type_init_finished($success, $results, $operations) {
  if ($success) {
    // We display the number of things we processed...
    drupal_set_message(t('All fields have been created and initialized.'));
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    drupal_set_message(t('An error occurred while processing @operation with arguments : @args', [
      '@operation' => $error_operation[0],
      '@args' => print_r($error_operation[0], TRUE),
    ]));
  }
}

/**
 * Creates a required instance of a lot quantity field on the specified bundle.
 *
 * @param string $field_name
 *   The name of the field; if it already exists, a new instance of the existing
 *   field will be created. For fields governed by the Commerce modules, this
 *   should begin with commerce_.
 * @param string $entity_type
 *   The type of entity the field instance will be attached to.
 * @param string $bundle
 *   The bundle name of the entity the field instance will be attached to.
 * @param string $label
 *   The label of the field instance.
 * @param int $weight
 *   The default weight of the field instance widget and display.
 */
function commerce_pom_admin_create_instance($field_name, $field_type, $required, $entity_type, $bundle, $label, $description = NULL, $weight = 0) {
  // Look for or add the specified lot quantity field to the requested entity
  // bundle.
  $field = field_info_field($field_name);
  $instance = field_info_instance($entity_type, $field_name, $bundle);

  if (empty($field)) {
    $field = [
      'field_name' => $field_name,
      'type' => $field_type,
      'cardinality' => 1,
      'entity_types' => [$entity_type],
      'translatable' => FALSE,
      'locked' => FALSE,
    ];
    $field = field_create_field($field);
  }

  if (empty($instance)) {
    $instance = [
      'field_name' => $field_name,
      'entity_type' => $entity_type,
      'bundle' => $bundle,
      'label' => $label,
      'required' => $required,
      'settings' => [],
      'display' => [],
      'description' => $description,
      'default_value' => [['value' => "1"]],
    ];

    $entity_info = entity_get_info($entity_type);

    // Spoof the default view mode so its display type is set.
    $entity_info['view modes']['default'] = [];

    field_create_instance($instance);
  }
}

/**
 * Ajax callback function for the bulk add products bundle display checkbox.
 */
function commerce_pom_bulk_add_products_config_ajax_callback($form, &$form_state) {
  return $form['product_options']['bulk_add_products'];
}
